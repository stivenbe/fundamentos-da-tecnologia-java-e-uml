package Datas;

import java.util.Calendar;

public class Datas {
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		int era = cal.get(Calendar.ERA);
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        System.out.println("Current Date: " + cal.getTime());
        System.out.println("Era: " +era);
        System.out.println("Dia: " + day);
        System.out.println("M�s: " + month);
        System.out.println("Ano: " + year);
	}
}
