package Lists;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Stack;
import java.util.WeakHashMap;

public class TesteLists {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
		ArrayList arrayList = new ArrayList();
		LinkedList linkedList = new LinkedList();
		
		// LinkedList add
		long startTime = System.currentTimeMillis();

		for (int i = 0; i < 10000; i++) {
			linkedList.add(i);
		}
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		System.out.println("LinkedList add time: " + duration);

		// LinkedList get
		startTime = System.currentTimeMillis();

		for (int i = 0; i < 5000; i++) {
			linkedList.get(i);
		}
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;
		System.out.println("LinkedList get time: " + duration);

		// ArrayList add
		startTime = System.nanoTime();

		for (int i = 0; i < 100000; i++) {
			arrayList.add(i);
		}
		endTime = System.nanoTime();
		duration = endTime - startTime;
		System.out.println("ArrayList add time:  " + duration);

		// ArrayList get
		startTime = System.nanoTime();

		for (int i = 0; i < 10000; i++) {
			arrayList.get(i);
		}
		endTime = System.nanoTime();
		duration = endTime - startTime;
		System.out.println("ArrayList get time:  " + duration);
		
		//-----------------------------------------------------
		Stack stack = new Stack();
		HashSet<Integer> hashSet = new HashSet<Integer>();
		
		// Stack add
		startTime = System.currentTimeMillis();
		
		for (int i = 0; i < 10000; i++){
			stack.push(i);
		}
		
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;
		System.out.println("Stack push time:  " + duration);
		
		//Stack pop
		startTime = System.currentTimeMillis();
		
		for (int i = 0; i < 5000; i++){
			stack.pop();
		}
		
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;
		System.out.println("Stack pop time:  " + duration);
		
		//HashSet add
		startTime = System.currentTimeMillis();
		
		for (int i = 0; i < 10000; i++){
			hashSet.add(i);
		}
		
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;
		System.out.println("HashSet add time:  " + duration);
		
		//HashSet remove
		startTime = System.currentTimeMillis();
		
		for (int i = 0; i < 5000; i++){
			hashSet.remove(i);
		}
		
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;
		System.out.println("HashSet remove time:  " + duration);
		
		//---------------------------------------------------------
		
		HashMap<Integer, String> hashMap = new HashMap<>();
		WeakHashMap<Integer, String> weakHash = new WeakHashMap<>();
		
		//Hashmap put
		startTime = System.currentTimeMillis();
		
		for (int i = 0; i < 10000; i++){
			hashMap.put(i, "a");
		}
		
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;
		System.out.println("HashMap put time:  " + duration);
		
		//Hashmap remove
		startTime = System.currentTimeMillis();
		
		for (int i = 0; i < 5000; i++){
			hashMap.remove(i, "a");
		}
		
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;
		System.out.println("HashMap remove time:  " + duration);
		
		//WeakHashmap put
		startTime = System.currentTimeMillis();
		
		for (int i = 0; i < 10000; i++){
			weakHash.put(i, "a");
		}
		
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;
		System.out.println("WeakHashmap put time:  " + duration);
		
		//WeakHashmap remove
		startTime = System.currentTimeMillis();
		
		for (int i = 0; i < 5000; i++){
			weakHash.remove(i, "a");
		}
		
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;
		System.out.println("WeakHashmap remove time:  " + duration);
		

	}
}
