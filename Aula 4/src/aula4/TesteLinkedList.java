package aula4;

import java.util.ArrayList;
import java.util.LinkedList;

public class TesteLinkedList {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
		ArrayList arrayList = new ArrayList();
		LinkedList linkedList = new LinkedList();
	
		// LinkedList add
		long startTime = System.currentTimeMillis();

		for (int i = 0; i < 10000; i++) {
			linkedList.add(i);
		}
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		System.out.println("LinkedList add time: " + duration);

		// LinkedList get
		startTime = System.currentTimeMillis();

		for (int i = 0; i < 5000; i++) {
			linkedList.get(i);
		}
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;
		System.out.println("LinkedList get time: " + duration);

		// ArrayList add
		startTime = System.nanoTime();

		for (int i = 0; i < 100000; i++) {
			arrayList.add(i);
		}
		endTime = System.nanoTime();
		duration = endTime - startTime;
		System.out.println("ArrayList add time:  " + duration);

		startTime = System.nanoTime();

		for (int i = 0; i < 10000; i++) {
			arrayList.get(i);
		}
		endTime = System.nanoTime();
		duration = endTime - startTime;
		System.out.println("ArrayList get time:  " + duration);

	}
}
