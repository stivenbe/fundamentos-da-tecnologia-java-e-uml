package Pessoa;

import java.util.ArrayList;

public class TestePessoas {

	public static void main(String[] args) {
		ArrayList<Pessoa> pessoas = new ArrayList<>();
		Pessoa p = new Pessoa();
		p.setNome("Stiven");
		p.setIdade(21);
		p.setSexoType(SexoType.MASCULINO);
		p.setEmpresa("Duxus");
		Pessoa p2 = new Pessoa();
		p2.setNome("Maria");
		p2.setIdade(41);
		p2.setSexoType(SexoType.FEMININO);
		p2.setEmpresa("Simples");
		Pessoa p3 = new Pessoa();
		p3.setNome("Henrique");
		p3.setIdade(40);
		p3.setSexoType(SexoType.MASCULINO);
		p3.setEmpresa("Magazine");
		
		pessoas.add(p);
		pessoas.add(p2);
		pessoas.add(p3);
		
		System.out.println();
	}
}
