package Pessoa;

public class Pessoa {
	private String nome;
	private int idade;
	private SexoType sexoType;
	private String empresa;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public SexoType getSexoType() {
		return sexoType;
	}

	public void setSexoType(SexoType sexoType) {
		this.sexoType = sexoType;
	}

}
