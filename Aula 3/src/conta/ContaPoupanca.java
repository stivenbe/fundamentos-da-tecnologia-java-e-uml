package conta;

import java.time.LocalDate;
import java.time.Period;

public class ContaPoupanca extends ContaBancaria{

	public ContaPoupanca(){
		super();
	}
	
	public ContaPoupanca(String nomeCliente, String endCliente, String cpfCliente) {
		super(nomeCliente, endCliente, cpfCliente);
	}

	@Override
	public void saque(double valor) throws SaldoInsuficienteException{
		if(Period.between(getCriacaoConta(), LocalDate.now()).getYears() < 1){
			valor +=10;
		}else{
			System.out.println("N�o h� taxa");
		}
		
		super.saque(valor);
	}
	
	@Override
	public void deposita(double valor) {
		super.deposita(valor);
	}
}
