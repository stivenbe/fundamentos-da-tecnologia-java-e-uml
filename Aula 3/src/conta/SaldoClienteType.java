package conta;

public enum SaldoClienteType {

	POTENCIAL,
	MEDIO,
	BAIXO;
	
	private double valor;
	private double valor2;
	
	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public double getValor2() {
		return valor2;
	}

	public void setValor2(double valor2) {
		this.valor2 = valor2;
	}
	
	public static SaldoClienteType isCompatible(double valor){
		
		if(valor >= 400.0 && valor <= 500.0){
			return SaldoClienteType.POTENCIAL;
		}else if(valor >= 200.0 && valor < 400.0){
			return SaldoClienteType.MEDIO;
		}else{
			return SaldoClienteType.BAIXO;
		}
	}
}
