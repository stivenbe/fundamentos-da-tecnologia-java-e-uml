package conta;

import java.time.LocalDate;
import java.time.Month;

public class TesteConta {
	public static void main(String[] args) throws SaldoInsuficienteException {

		ContaEspecial ce = new ContaEspecial();
		ce.setNomeCliente("Stiven Ce");
		ce.setEndCliente("Cotia Ce");
		ce.setCpfCliente("123 Ce");

		ContaEspecial ce1 = new ContaEspecial();
		ce1.setNomeCliente("Stiven Ce1");
		ce1.setEndCliente("Cotia Ce1");
		ce1.setCpfCliente("123 Ce1");

		ContaEspecial ce2 = new ContaEspecial();
		ce2.setNomeCliente("Stiven Ce1");
		ce2.setEndCliente("Cotia Ce1");
		ce2.setCpfCliente("123 Ce1");
		
		ContaPoupanca cp = new ContaPoupanca();
		cp.setNomeCliente("Stiven cp");
		cp.setEndCliente("Cotia cp");
		cp.setCpfCliente("123 cp");
		cp.setDataNasc(LocalDate.of(1995, Month.DECEMBER, 28));
		cp.setCriacaoConta(LocalDate.of(2017, Month.SEPTEMBER, 29));

		System.out.println("-------------------------------------------");

		 System.out.println("CONTA ESPECIAL CE");
		 ce.deposita(600.00);
		 ce.saque(100.00);
		
		 System.out.println("Nome: " +ce.getNomeCliente());
		 System.out.println("Endere�o: " +ce.getEndCliente());
		 System.out.println("CPF: " +ce.getCpfCliente());
		 System.out.println("Saldo: " +ce.getSaldo());
		 System.out.println("Tipo Cliente: " +SaldoClienteType.isCompatible(ce.getSaldo()));
		
		 System.out.println("-------------------------------------------");
		
		 System.out.println("CONTA ESPECIAL CE1");
		 ce1.deposita(200.00);
		 ce1.saque(100.00);
		
		 System.out.println("Nome: " +ce1.getNomeCliente());
		 System.out.println("Endere�o: " +ce1.getEndCliente());
		 System.out.println("CPF: " +ce1.getCpfCliente());
		 System.out.println("Saldo: " +ce1.getSaldo());
		 System.out.println("Tipo Cliente:"
		 +SaldoClienteType.isCompatible(ce1.getSaldo()));
		 
		 System.out.println("-------------------------------------------");
			
		 System.out.println("CONTA POUPANCA CP");
		 cp.deposita(200.00);
		 cp.saque(100.00);
		
		 System.out.println("Nome: " +cp.getNomeCliente());
		 System.out.println("Endere�o: " +cp.getEndCliente());
		 System.out.println("CPF: " +cp.getCpfCliente());
		 System.out.println("Saldo: " +cp.getSaldo());
		 System.out.println("Tipo Cliente:" +SaldoClienteType.isCompatible(cp.getSaldo()));
		 System.out.println("Data Nascimento: " +cp.getDataNasc());
		 System.out.println("Cria��o de conta: " +cp.getCriacaoConta());

		System.out.println("-------------------------------------------");

		System.out.println("CONTA ESPECIAL CE1 EXCEPTION");
		// ce1.deposita(200.00);
		try {
			ce2.saque(100.00);

			System.out.println("Nome: " + ce2.getNomeCliente());
			System.out.println("Endere�o: " + ce2.getEndCliente());
			System.out.println("CPF: " + ce2.getCpfCliente());
			System.out.println("Saldo: " + ce2.getSaldo());
			System.out.println("Tipo Cliente:" + SaldoClienteType.isCompatible(ce2.getSaldo()));

		} catch (SaldoInsuficienteException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println("Generica" +e.getMessage());
		}
	}
}
