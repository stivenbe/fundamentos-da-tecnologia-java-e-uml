package conta;

import java.time.LocalDate;

public abstract class ContaBancaria {
	protected double saldo;
	protected String nomeCliente;
	protected String endCliente;
	protected String cpfCliente;
	protected LocalDate dataNasc;
	protected LocalDate criacaoConta;
	
	public ContaBancaria(){}
	
	public ContaBancaria(String nomeCliente, String endCliente, String cpfCliente) {
		this.nomeCliente = nomeCliente;
		this.endCliente = endCliente;
		this.cpfCliente = cpfCliente;
	}

	public void saque(double valor) throws SaldoInsuficienteException {
		if((saldo - valor) < 0)
			throw new SaldoInsuficienteException("Saldo Insuficiente! \n" + "Seu saldo � de: " +saldo);
		
		saldo -= valor;
	}

	public void deposita(double valor) {
		saldo += valor;
	}

	public double getSaldo() {
		return saldo;
	}
		
	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getEndCliente() {
		return endCliente;
	}

	public void setEndCliente(String endCliente) {
		this.endCliente = endCliente;
	}

	public String getCpfCliente() {
		return cpfCliente;
	}

	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}
	
	public LocalDate getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(LocalDate dataNasc) {
		this.dataNasc = dataNasc;
	}

	public LocalDate getCriacaoConta() {
		return criacaoConta;
	}

	public void setCriacaoConta(LocalDate criacaoConta) {
		this.criacaoConta = criacaoConta;
	}

	public double getTaxaBancaria(){
		return 1.0;
	}
	
	public double getTaxaBancaria(int dias){
		return getTaxaBancaria() * dias;
	}
	
	public double getTaxaBancaria(int dias, int meses){
		return getTaxaBancaria(dias+meses*30);
	}
}
