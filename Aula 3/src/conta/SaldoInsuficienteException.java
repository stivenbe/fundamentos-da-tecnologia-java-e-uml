package conta;

public class SaldoInsuficienteException extends Exception {
	
	private static final long serialVersionUID = -493987379955501L;

	public SaldoInsuficienteException(String msg){
		super(msg);
	}
}
