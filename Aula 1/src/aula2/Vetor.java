package aula2;

import java.util.Random;

public class Vetor {
	public static void main(String[] args) {
		int[] valores = new int[5]; // vetor de 5 posi��es
		int maior = 0;
		int menor = 0;
		int soma = 0;
		double media;
//		for (int i = 0; i < 5; i++) {
//			System.out.println("Informe o valor do " + (i + 1) + "� valor: ");
//			valores[i] = new Scanner(System.in).nextInt();
//		}
	    
		//imprime sequ�ncia de 5 n�meros inteiros aleat�rios
	    for (int i = 0; i < valores.length; i++) {
	    	valores[i] = new Random().nextInt(51);
	    	System.out.println("Informe o valor do " + (i + 1) + "� valor: " +valores[i]);
	 	}
		// Add os primeiros valores �s vari�veis
		menor = valores[0];
		maior = valores[0];
		for (int i = 0; i < valores.length; i++) {
			soma += valores[i]; // soma os valores
			if (menor > valores[i])
				menor = valores[i]; // Verifica e add o menor valor
			if (maior < valores[i])
				maior = valores[i]; // Verifica e add o maior valor
		}
		// Calcula a m�dia
		media = soma / valores.length;
		// print para mostrar os valores, soma e m�dia...
		System.out.println("Soma dos valores: " +soma);
		System.out.println("-----");
		System.out.println("Media dos valores: " +media);
		System.out.println("-----");
		System.out.println("Menor valor: " +menor);
		System.out.println("-----");
		System.out.println("Maior valor: " +maior);
	}
}
