package aula2;

public class Conversor {
	public static void main(String[] args) {
		System.out.println("Binario: " + converteDecimalParaBinario(12));
		System.out.println("===");
		System.out.println("Decimal: " + converteBinarioParaDecimal("1100"));
	}

	public static String converteDecimalParaBinario(int valor) {
		int resto = -1;
		StringBuilder sb = new StringBuilder();
		
		if (valor == 0) {
			return "0";
		}

		while (valor > 0) {
			resto = valor % 2;
			valor /= 2;
			sb.insert(0, resto);
			System.out.println(sb);
		}

		return sb.toString();
	}

	public static int converteBinarioParaDecimal(String valorBinario) {
		int valor = 0;

		for (int i = valorBinario.length(); i > 0; i--) {
			valor += Integer.parseInt(valorBinario.charAt(i - 1) + "") * Math.pow(2, (valorBinario.length() - i));
		}

		return valor;
	}
}
