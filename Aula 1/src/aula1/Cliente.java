package aula1;

public class Cliente {
	private String RG;
	private String CPF;
	private String nome;
	private String endereco;
		
	public Cliente() {}
	
	public Cliente(String rG) {
		this();
		this.RG = rG;
	}

	public Cliente(String rG, String cPF, String nome, String endereco) {
		this(rG);
		this.CPF = cPF;
		this.nome = nome;
		this.endereco = endereco;
	}

	public String getRG() {
		return RG;
	}
	public void setRG(String rG) {
		RG = rG;
	}
	public String getCPF() {
		return CPF;
	}
	public void setCPF(String cPF) {
		CPF = cPF;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	
}
