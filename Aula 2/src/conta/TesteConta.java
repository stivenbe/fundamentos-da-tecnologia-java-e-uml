package conta;

public class TesteConta {
	public static void main(String[] args) {
			
		ContaBancaria cb = new ContaBancaria();
		cb.setNomeCliente("Stiven");
		cb.setEndCliente("Cotia");
		cb.setCpfCliente("123");
		
		ContaEspecial ce = new ContaEspecial();
		ce.setNomeCliente("Stiven Ce");
		ce.setEndCliente("Cotia Ce");
		ce.setCpfCliente("123 Ce");
		
		ContaEspecial ce1 = new ContaEspecial();
		ce1.setNomeCliente("Stiven Ce1");
		ce1.setEndCliente("Cotia Ce1");
		ce1.setCpfCliente("123 Ce1");
		
		System.out.println("CONTA BANCARIA");
		cb.deposita(300.00);
		cb.saque(100.00);
		
		System.out.println("Nome: " +cb.getNomeCliente());
		System.out.println("Endere�o: " +cb.getEndCliente());
		System.out.println("CPF: " +cb.getCpfCliente());
		System.out.println("Saldo: " +cb.getSaldo());
		System.out.println("Tipo Cliente: " +SaldoClienteType.isCompatible(cb.getSaldo()));
		
		System.out.println("-------------------------------------------");
		
		System.out.println("CONTA ESPECIAL CE");
		ce.deposita(600.00);
		ce.saque(100.00);
		
		System.out.println("Nome: " +ce.getNomeCliente());
		System.out.println("Endere�o: " +ce.getEndCliente());
		System.out.println("CPF: " +ce.getCpfCliente());
		System.out.println("Saldo: " +ce.getSaldo());
		System.out.println("Tipo Cliente:" +SaldoClienteType.isCompatible(ce.getSaldo()));
		
		System.out.println("-------------------------------------------");
		
		System.out.println("CONTA ESPECIAL CE1");
		ce1.deposita(200.00);
		ce1.saque(100.00);
		
		System.out.println("Nome: " +ce1.getNomeCliente());
		System.out.println("Endere�o: " +ce1.getEndCliente());
		System.out.println("CPF: " +ce1.getCpfCliente());
		System.out.println("Saldo: " +ce1.getSaldo());
		System.out.println("Tipo Cliente:" +SaldoClienteType.isCompatible(ce1.getSaldo()));
	}
}
