package conta;

public class ContaEspecial extends  ContaBancaria{
	
	public ContaEspecial(){
		super();
	}
	
	public ContaEspecial(String nomeCliente, String endCliente, String cpfCliente) {
		super(nomeCliente, endCliente, cpfCliente);
	}

	@Override
	public void saque(double valor) {
		super.saque(valor*.9);
	}
	
	@Override
	public void deposita(double valor) {
		super.deposita(valor*.9);
	}
}
