package Calculadora;

public class Calculadora implements CalculadoraBasica{

	@Override
	public float somar(float op1, float op2) {
		float soma = op1 + op2;
		return soma;
	}

	@Override
	public float subtrair(float op1, float op2) {
		float subtrair = op1 - op2;
		return subtrair;
	}

	@Override
	public float dividir(float op1, float op2) {
		float divisao = 0;
		
		if (op1 <= 0 || op2 <= 0){
			System.out.println("N�o � divisivel por 0");
		}else{
			divisao = op1/op2;
		}
		return divisao;
	}

	@Override
	public float multiplicar(float op1, float op2) {
		float multi = op1 * op2;
		return multi;
	}
}
