package Calculadora;

import java.util.Scanner;

public class TesteCalc {
	private static Scanner input1;

	public static void main(String[] args) {
		
		Calculadora c = new Calculadora();
		
		int opcao = 5;
		float op1;
		float op2;
	    input1 = new Scanner(System.in);  
		System.out.println("-Escolha uma op��o-");  
        System.out.println("1. Soma");    
        System.out.println("2. Subtracao");    
        System.out.println("3. Divisao");    
        System.out.println("4. Multiplica��o");    
        System.out.println("0. Sair");    
        System.out.println("Opera��o: ");    
        opcao = input1.nextInt();  
        while (opcao != 0) {

		System.out.println("Qual o primeiro numero: ");
		 op1 = input1.nextInt();
		System.out.println("Qual o segundo numero: ");
		 op2 = input1.nextInt();
		 
		 if ( opcao == 1 ) {
				float operacao =  c.somar(op1, op2);
             System.out.println("Soma �: " +operacao);  
             break;  
         }  
         else if (opcao == 2) { 
        	 float operacao = c.subtrair(op1, op2);
				System.out.printf("Subtra��o �: ", operacao);  
				break;  
			}  
			else if (opcao == 3) {
				float operacao = c.dividir(op1, op2); 
				System.out.printf("Divis�o �: ", operacao);    
				break;  
			}  
			else if (opcao == 4) {  
				float operacao = c.multiplicar(op1, op2);  
				System.out.printf("Multiplica��o �: ", operacao);   
				break;  
			}  
			else{  
				System.out.println("Saindo..");  
				break;  
			}      
        }
		
	}
}
