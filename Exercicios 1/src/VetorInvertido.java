import java.util.Random;
import java.util.Scanner;

public class VetorInvertido {
	
	private static Scanner x;

	public static void main(String[] args) {

		x = new Scanner(System.in);
		System.out.println("informe quantidade de valores: ");
		System.out.println();
		int qtd = x.nextInt();
		int a[] = new int[qtd];
		// o vetor b � o inverso de a. Essa declara��o � feita no metodo mais abaixo 'inverte'. 
		int b[] = new int[qtd];
		//Gera o vetor aleatorio de acordo com as posi��es entradas.
	    for (int i = 0; i < a.length; i++) {
	    	a[i] = new Random().nextInt(51);
	    	System.out.println("Informe o valor do " + (i + 1) + "� valor: " +a[i]);
	 	}
		inverte(a, b, qtd);
	}

	public static void inverte(int a[], int b[], int qtd) {
		// aqui os valores s�o invertidos
		int j = qtd - 1;
		for (int i = 0; i < qtd; i++) {
			b[i] = a[j];
			j -= 1;
		}
		//imprime o valor invertido
		for (int i = 0; i < qtd; i++) {
			System.out.println("Vetor Invertido: " + b[i]);
		}
	}
}
