import java.util.Scanner;

public class Calculadora {
	private static Scanner input;

	public static void main(String args[]) {
		// criando um objeto c a apartir do metodo calc
		Calculadora c = new Calculadora();
		// declarando as var�aveis
		int opcao = 5;
		int num1;
		int num2;
		input = new Scanner(System.in);
		System.out.println("-Escolha uma op��o-");
		System.out.println("1. Soma");
		System.out.println("2. Subtracao");
		System.out.println("3. Multiplicacao");
		System.out.println("4. Divisao");
		System.out.println("0. Sair");
		System.out.println("Opera��o: ");
		opcao = input.nextInt();
		while (opcao != 0) {
			System.out.println("Qual o primeiro numero: ");
			num1 = input.nextInt();
			System.out.println("Qual o segundo numero: ");
			num2 = input.nextInt();
			if (opcao == 1) {
				int operacao = c.som(num1, num2);
				System.out.printf("\nO resultado da soma �: %d\n", operacao);
				break;
			} else if (opcao == 2) {
				int operacao = c.sub(num1, num2);
				System.out.printf("\nO resultado da subtra��o �: %d\n", operacao);
				break;
			} else if (opcao == 3) {
				int operacao = c.mult(num1, num2);
				System.out.printf("\nO resultado da multiplica��o �: %d\n", operacao);
				break;
			} else if (opcao == 4) {
				try {
					int operacao = c.div(num1, num2);
					System.out.printf("\nO resultado da divis�o �: %d\n", operacao);
					break;
				} catch (ArithmeticException e) {
					System.out.println(e.getMessage());
				}
				continue;
			} else {
				System.out.println("Saindo..");
				break;
			}
		}
	}

	public int som(int num1, int num2) {
		return num1 + num2;
	}

	public int sub(int num1, int num2) {
		return num1 - num2;
	}

	public int div(int num1, int num2) throws ArithmeticException {
		if (num1 <= 0 || num2 <= 0)
			throw new ArithmeticException("N�o pode ser dividido por 0");

		return num1 / num2;
	}

	public int mult(int num1, int num2) {
		return num1 * num2;
	}
}
